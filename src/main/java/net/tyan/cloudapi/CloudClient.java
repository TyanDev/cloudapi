package net.tyan.cloudapi;


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import net.tyan.cloudapi.networking.ClientChannelHandler;
import net.tyan.cloudapi.reference.Operations;
import net.tyan.cloudapi.reference.Reference;
import net.tyan.cloudapi.tasks.InputTask;

public class CloudClient {

    public Channel channel;
    public NioEventLoopGroup child;

	public static void main(String[] args) {
        CloudAPI.send(Operations.TEST_CONNECTION);
	}

    public CloudClient() {
        child = new NioEventLoopGroup();

        try {
            Bootstrap bootstrap = new Bootstrap()
                    .group(child)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            ChannelPipeline pipeline = channel.pipeline();
                            pipeline.addLast(new ClientChannelHandler());
                        }
                    });
            this.channel = bootstrap.connect(Reference.HOST, Reference.PORT).sync().channel();

            Thread inputTask = new Thread(new InputTask(this));
            inputTask.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}