package net.tyan.cloudapi;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.tyan.cloudapi.reference.Operations;

/**
 * Created by Kevin
 */

public class CloudAPI {

    private static boolean isInitialized = false;
    private static CloudClient client;

    public static void initAPI() {
        client = new CloudClient();
        isInitialized = true;
    }

    public static void send(Operations operation) {
        if (!isInitialized) initAPI();
        int id = operation.getId();
        ByteBuf byteBuf = Unpooled.buffer();
        byteBuf.writeInt(id);
        client.channel.writeAndFlush(byteBuf);
    }

}
