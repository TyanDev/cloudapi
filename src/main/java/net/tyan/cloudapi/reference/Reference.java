package net.tyan.cloudapi.reference;

/**
 * Created by Kevin on 08.08.2015.
 */
public class Reference {

    public static final String HOST = "46.105.17.57";
    public static final int PORT = 2020;

    public static final String TEAMSPEAK_SCRIPT = "/home/teamspeak/ts3server_startscript.sh";
}
