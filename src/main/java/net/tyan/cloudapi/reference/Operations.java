package net.tyan.cloudapi.reference;

/**
 * Created by Kevin
 */

public enum Operations {
    EXIT(-1),
    CLOSE(0),
    TEST_CONNECTION(1),

    RESTART_TEAMSPEAK(2);


    private int id;

    Operations(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public static Operations getById(int id) {
        for (Operations task : values()) {
            if (task.id == id)
                return task;
        }
        return null;
    }

}
