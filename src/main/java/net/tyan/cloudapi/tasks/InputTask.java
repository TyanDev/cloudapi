package net.tyan.cloudapi.tasks;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.tyan.cloudapi.CloudClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Kevin
 */

public class InputTask implements Runnable {

    private CloudClient client;
    private boolean running;

    public InputTask(CloudClient client) {
        this.client = client;
        this.running = true;
    }

    @Override
    public void run() {
        try {
            while (running) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String input = reader.readLine();
                if (input == null)
                    break;
                ByteBuf byteBuf = Unpooled.buffer();
                if (input.equalsIgnoreCase("exit")) {
                    byteBuf.writeInt(-1);
                    client.channel.writeAndFlush(byteBuf);
                    client.channel.closeFuture().sync();
                    this.running = false;
                } else if (input.equalsIgnoreCase("close")) {
                    byteBuf.writeInt(0);
                    client.channel.writeAndFlush(byteBuf);
                    client.channel.closeFuture().sync();
                    this.running = false;
                } else if (input.equalsIgnoreCase("test")) {
                    byteBuf.writeInt(1);
                    client.channel.writeAndFlush(byteBuf);
                } else if (input.equalsIgnoreCase("restart_ts3")) {
                    byteBuf.writeInt(2);
                    client.channel.writeAndFlush(byteBuf);
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            client.child.shutdownGracefully();
        }
    }
}
