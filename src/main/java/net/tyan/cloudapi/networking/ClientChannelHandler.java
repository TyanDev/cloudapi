package net.tyan.cloudapi.networking;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.tyan.cloudapi.CloudClient;
import net.tyan.cloudapi.reference.Operations;
import net.tyan.cloudapi.tasks.InputTask;

/**
 * Created by Kevin
 */

public class ClientChannelHandler extends SimpleChannelInboundHandler<ByteBuf> {

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, ByteBuf buf) throws Exception {
        int id = buf.readInt();
        Operations task = Operations.getById(id);
        if (task == null) return;
        switch (task) {
            case TEST_CONNECTION:
                System.out.println("Connection is established!");
                break;
            case CLOSE:
                System.out.print("Server closed!");
                ctx.close();
                break;
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("Joined the server!");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause.getMessage());
    }
}
